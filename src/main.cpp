#include "app.h"

// https://stackoverflow.com/questions/16150211/unresolved-externals-trying-to-use-ffmpeg/16150305
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}
// Looking for a BGFX tutorial? This is really helpful!
// Though see the comments for udpates.
// https://dev.to/pperon/hello-bgfx-4dka
// Also see
// https://github.com/yuki-koyama/hello-bgfx

// For an FFmpeg tutorial, I'm looking at
// An ffmpeg and SDL Tutorial, or, How to Write a Video Player in Less Than
// 1000 Lines, http://dranger.com/ffmpeg/ffmpeg.html.
// It mentions that it's based on the higher-performance ffplay.c, which might
// be worth looking at.

int main(void) {
  // Perform one-time FFmpeg initialization
  /*av_register_all(); // Initialize libavformat and register everything

  // PROTOTYPE
  AVFormatContext *pFormatCtx = nullptr;

  // Open the video file
  const char *filename =
      "C:\\Users\\techi\\Videos\\2020-05-03 19-01-17_Trim.mp4";
  if (avformat_open_input(
          &pFormatCtx,
          filename,
          nullptr, nullptr) != 0) {
    return -1; // Couldn't open file
  }

  // Retrieve stream information (populating pFormatCtx->streams)
  if (avformat_find_stream_info(pFormatCtx, nullptr) < 0) {
    return -1; // Couldn't find stream information
  }

  // Dump information about stream 0 onto standard error
  av_dump_format(pFormatCtx, 0, filename, 0);

  // Walk through pFormatCtx->streams until we find a video stream
  int videoStream = -1;
  for (int i = 0; i < pFormatCtx->nb_streams; i++) {
    if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
      videoStream = i;
      break;
    }
  }
  if (videoStream == -1) {
    return -1; // Didn't find a video stream
  }

  // Get a pointer to the codec context for the video stream
  AVCodecContext *pCodecCtxOrig = pFormatCtx->streams[videoStream]->codec;
  // Find a decoder for the video stream
  AVCodec *pCodec = avcodec_find_decoder(pCodecCtxOrig->codec_id);
  if (pCodec == nullptr) {
    return -1; // Codec not found
  }
  // Copy context
  AVCodecContext *pCodecCtx = avcodec_alloc_context3(pCodec);
  if (avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0) {
    return -1; // Error copying codec context
  }
  // Open codec
  if (avcodec_open2(pCodecCtx, pCodec, nullptr) < 0) {
    return -1; // Could not open codec
  }

  // Store the data

  // Allocate video frame
  AVFrame *pFrame = av_frame_alloc();
  // Allocate a frame for the converted frame
  AVFrame *pFrameRGB = av_frame_alloc();
  if (pFrameRGB == nullptr) {
    return -1;
  }

  // Allocate space for the raw data
  int numBytes =
      avpicture_get_size(AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
  uint8_t *buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t)); // av_malloc is just an aligned malloc
  
                                                                      // Assign appropriate parts of buffer to image planes in pFrameRGB
  av_image_fill_arrays(pFrameRGB->data, pFrameRGB->linesize, buffer,
                       AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height,
                       1);

  // Read the data by reading video packets until our frame is complete
  SwsContext *sws_ctx =
      sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt,
                     pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_RGB24,
                     SWS_BILINEAR, nullptr, nullptr, nullptr);
  int frameFinished;
  AVPacket packet;
  while (av_read_frame(pFormatCtx, &packet) >= 0) {
    // Is this a packet from the video stream?
    if (packet.stream_index == videoStream) {
      // Decode video frame
      avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

      // Did we finish the frame?
      if (frameFinished) {
        // Convert from native format to RGB
        sws_scale(sws_ctx, pFrame->data, pFrame->linesize, 0, pCodecCtx->height,
                  pFrameRGB->data, pFrameRGB->linesize);

        // Save the frame to disk
        char *outFilename = "out.ppm";

        FILE *pFile = fopen(outFilename, "wb");
        if (pFile != nullptr) {
            // Write header
          fprintf(pFile, "P6\n%d %d\n255\n", pCodecCtx->width, pCodecCtx->height);

          // Write pixel data
          for (int y = 0; y < pCodecCtx->height; y++) {
            fwrite(pFrameRGB->data[0] + y * pFrameRGB->linesize[0], 1, pCodecCtx->width * 3,
                   pFile);
          }
          
          // Close file
          fclose(pFile);
        }
        break;
      }
    }
  }*/
  // END PROTOTYPE

  // Start Gridby
  gridby::App app;
  app.Run();

  // PROTOTYPE
  /*av_free(buffer);
  av_free(pFrameRGB);
  av_free(pFrame);
  avcodec_close(pCodecCtx);
  avcodec_close(pCodecCtxOrig);
  avformat_close_input(&pFormatCtx);*/
  // END PROTOTYPE
}