#include "controls.h"

namespace gridby {

// This scroll callback function buffers scrolls into a y offset variable.
// Every time a Controls class updates, this variable will be set back to 0.
static double s_scrollOffsetY = 0.0;
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
  s_scrollOffsetY += yoffset;
}

// Based on
// https://github.com/ocornut/imgui/blob/master/examples/imgui_impl_glfw.cpp
void key_callback(GLFWwindow *window, int key, int scancode, int action,
                  int mods) {

  ImGuiIO &io = ImGui::GetIO();
  if (action == GLFW_PRESS) {
    io.KeysDown[key] = true;
  }
  if (action == GLFW_RELEASE) {
    io.KeysDown[key] = false;
  }

  io.KeyCtrl =
      io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
  io.KeyShift =
      io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
  io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
#ifdef _WIN32
  io.KeySuper = false;
#else  // #ifdef _WIN32
  io.KeySuper =
      io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
#endif // #ifdef _WIN32
}

void character_callback(GLFWwindow *window, unsigned int codepoint) {
  ImGui::GetIO().AddInputCharacter(codepoint);
}

// Based on
// https://github.com/ocornut/imgui/blob/master/examples/imgui_impl_glfw.cpp

void Controls::Init(GLFWwindow *window) {
  glfwSetScrollCallback(window, scroll_callback);
  glfwSetKeyCallback(window, key_callback);
  glfwSetCharCallback(window, character_callback);

  ImGuiIO &io = ImGui::GetIO();
  io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
}

void Controls::Update(GLFWwindow *window) {
  m_lastXPos = m_xPos;
  m_lastYPos = m_yPos;
  glfwGetCursorPos(window, &m_xPos, &m_yPos);
  m_lastMouseButtons = m_mouseButtons;
  m_mouseButtons.lmbPressed =
      (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS);
  m_mouseButtons.mmbPressed =
      (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
  m_mouseButtons.rmbPressed =
      (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS);
  // Get and reset the scroll counter
  m_scrollOffsetY = s_scrollOffsetY;
  s_scrollOffsetY = 0.0;
  m_totalScrollY += m_scrollOffsetY;

  int newWindowWidth, newWindowHeight;
  glfwGetWindowSize(window, &newWindowWidth, &newWindowHeight);
  m_needsResize =
      (newWindowWidth != m_windowWidth) || (newWindowHeight != m_windowHeight);
  m_windowWidth = newWindowWidth;
  m_windowHeight = newWindowHeight;
}

} // namespace gridby