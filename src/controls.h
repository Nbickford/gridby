#pragma once

#include "GLFW/glfw3.h"
#include "imgui/imgui.h"

namespace gridby {

struct MouseButtonState {
  bool lmbPressed;
  bool mmbPressed;
  bool rmbPressed;
};

class Controls {
public:
  // Sets up GLFW's callback functions for the window, and Dear ImGui's key map.
  void Init(GLFWwindow *window);

  // Should be called at the start of each frame
  void Update(GLFWwindow *window);

  template <class T> T GetMouseX() const { return static_cast<T>(m_xPos); }

  template <class T> T GetMouseY() const { return static_cast<T>(m_yPos); }

  template <class T> T GetMouseDeltaX() const {
    return static_cast<T>(m_xPos - m_lastXPos);
  }

  template <class T> T GetMouseDeltaY() const {
    return static_cast<T>(m_yPos - m_lastYPos);
  }

  template <class T> T GetLastMouseX() const {
    return static_cast<T>(m_lastXPos);
  }

  template <class T> T GetLastMouseY() const {
    return static_cast<T>(m_lastYPos);
  }

  uint8_t GetImGuiMouseButtons() const {
    return (m_mouseButtons.lmbPressed ? IMGUI_MBUT_LEFT : 0) |
           (m_mouseButtons.mmbPressed ? IMGUI_MBUT_MIDDLE : 0) |
           (m_mouseButtons.rmbPressed ? IMGUI_MBUT_RIGHT : 0);
  }

  bool IsLMBJustPressed() const {
    return (m_mouseButtons.lmbPressed && (!m_lastMouseButtons.lmbPressed));
  }

  bool IsKeyJustPressed(uint32_t glfwKey) const {
    return ImGui::GetIO().KeysDownDuration[glfwKey] == 0.0f;
  }

  template <class T> T GetScrollOffsetY() const { return m_scrollOffsetY; }

  template <class T> T GetTotalScrollY() const { return m_totalScrollY; }

  template <class T> T GetWindowWidth() const {
    return static_cast<T>(m_windowWidth);
  }

  template <class T> T GetWindowHeight() const {
    return static_cast<T>(m_windowHeight);
  }

  // Aspect ratio = width / height
  float GetWindowAspectRatio() const {
    return static_cast<float>(m_windowWidth) /
           static_cast<float>(m_windowHeight);
  }

  bool GetNeedsResize() const { return m_needsResize; }

private:
  double m_xPos = 0.0;
  double m_yPos = 0.0;
  double m_lastXPos = 0.0; // X position on previous frame
  double m_lastYPos = 0.0; // Y position on previous frame
  MouseButtonState m_mouseButtons;
  MouseButtonState m_lastMouseButtons;
  double m_scrollOffsetY =
      0.0; // Accumulated scroll since the last call to Update
  double m_totalScrollY = 0.0; // Total scroll in Y
  int m_windowWidth = 0;
  int m_windowHeight = 0;
  bool m_needsResize = false; // If the window size was changed this frame
};

} // namespace gridby