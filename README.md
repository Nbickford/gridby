# Gridby

A grid-based editor for virtual orchestra videos. Currently in unstable prototype stage - this README includes some notes on the development process so far.

This project uses submodules for some of the projects it includes, such as BGFX. To update submodules for the first time after cloning the Gridby repository, run `git submodule update --init --recursive`.

## Compiling FFmpeg with OpenH264 using MSVC

I'm currently looking at using the LGPL version of FFmpeg together with OpenH264 in order to load video files. In order to do that, I had to build FFmpeg with OpenH264 on Windows! Here's how I did that - adapted from https://www.roxlu.com/2019/062/compiling-ffmpeg-with-x264-on-windows-10-using-msvc. It takes a while to work out, but it's manageable! Note that I'm not 100% sure that the OpenH264 integration step worked, since the generated `ffmpeg.exe` doesn't seem to have any references to `openh264.dll`, so far as I can tell.

* Download `msys2` from https://www.msys2.org. I installed it to a folder named "msys2". Note that this folder has to be writable! It's going to be (in some sense) the local filesystem for this.
* Open an x64 Native Tools Command Prompt for VS 2019 and run `msys2\msys2_shell.cmd -use-full-path`. We'll be entering commands into the MSYS2 shell for the rest of this build process.
* Install dependencies and rename `link.exe` (which otherwise conflicts with MSVC's link.exe) by entering:

```
pacman -Syu
pacman -S make diffutils yasm nasm
mv /usr/bin/link.exe /usr/bin/link.exe.bak
```

* Get the sources for OpenH264 and FFmpeg:

```
mkdir tmp
cd tmp
mkdir sources
mkdir build
mkdir installed
cd sources

git clone --depth 1 https://github.com/cisco/openh264.git
git clone --depth 1 git://source.ffmpeg.org/ffmpeg.git
```

(the `--depth 1` means `git` only checks out the latest version of the source code, instead of the entire history of the repository.)

* Compile OpenH264 (this will create a set of static libraries and some other files):

```
cd openh264
make install PREFIX=../../installed OS=msvc ARCH=x86_64
```

Now add OpenH264 to `pkg-config` so that `ffmpeg` can find it:

```
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:~/tmp/installed/lib/pkgconfig
```

You can make sure pkg-config can find OpenH264 by running
```
pkg-config --exists --print-errors openh264
```
If it doesn't print out any errors, then pkg-config can find it.

* Configure FFmpeg (under the LGPL):

```
cd ../../build
```
(you should now be in tmp/build)
```
mkdir ffmpeg
cd ffmpeg

export CC=cl

./../../sources/ffmpeg/configure \
             --prefix=./../../installed \
             --target-os=win64 \
             --arch=x86_64 \
             --toolchain=msvc \
             --enable-libopenh264 \
             --disable-static \
             --enable-shared \
             --extra-ldflags="-LIBPATH:./../../installed/lib/" \
             --extra-cflags="-I./../../installed/include/"
```
Then wait while configure works. (You can see its log in ffmpeg/ffbuild). It'll eventually print out information about the current configuration!

* Finally, build FFmpeg:

```
make V=1 -j
make install
```

For development on Gridby, you will then need to copy `installed/bin` and `installed/include` into Gridby's `ffmpeg` folder, and a zipped version of `sources/ffmpeg` into the `gridby/ffmpeg` folder. Finally, copy `installed/lib/openh264_dll.lib` into `gridby/ffmpeg/bin`. Including the zipped version of the FFmpeg sources used with all releases ensures Gridby complies with the LGPL that FFmpeg was built with.

## License Information

Gridby uses version 2.1 of the Hippocratic License. See the `LICENSE` file for more information.

Gridby also uses a number of open source projects. Currently, it uses:

* BGFX, licensed under the BSD 2-Clause license (https://github.com/bkaradzic/bgfx/blob/master/LICENSE)
* FFmpeg built without `--enable-gpl` or `--enable-nonfree`, licensed under the LGPL 2.1 (http://ffmpeg.org/legal.html, http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html). While no builds of Gridby have been released as of this writing, the source code for FFmpeg that was used to build the DLL in the `ffmpeg` directory is available as `ffmpeg/ffmpeg.zip`.
* GLFW, licensed under the zlib/libpng license (https://www.glfw.org/license.html)
* OpenH264, licensed under the BSD 2-Clause license (https://github.com/cisco/openh264/blob/master/LICENSE)