#pragma once

#include "controls.h"
#include "gridbyMath.h"

namespace gridby {

// A simple orthogonal camera that looks in the +Z direction and scrolls over
// the XY plane. (Gridby internally uses a right-handed coordinate system.)
class PlanarCamera {
public:
  // Gives the camera information about properties that should only need to be
  // set once. These properties can be obtained from bgfx::Caps.
  void Configure(bool homogeneousDepth, bool originBottomLeft) {
    m_isHomogeneousDepth = homogeneousDepth;
    m_isOriginBottomLeft = originBottomLeft;
    RecomputeMatrices();
  }

  // Sets the location of the center of the camera.
  void SetPosition(Vec2 origin) {
    m_origin = origin;
    RecomputeMatrices();
  }

  // Informs the camera of the resolution it outputs to. This allows the camera
  // to convert between movements in pixels and movements in GWS, and to compute
  // its aspect ratio (pixels are assumed square).
  void SetOutputSize(PointPixels outputSize) {
    m_outputSize = outputSize;
    RecomputeMatrices();
  }

  // Sets the size of the camera in world-space along the Y axis.
  void SetYExtent(float worldSpaceHeight) {
    m_yExtentWS = worldSpaceHeight;
    RecomputeMatrices();
  }

  void SetZBounds(float zMin, float zMax) {
    m_zMin = zMin;
    m_zMax = zMax;
  }

  const Matrix &GetViewMatrix() { return m_view; }

  const Matrix &GetProjectionMatrix() { return m_proj; }

  // Moves the camera according to the given controls.
  void InputControls(const Controls &controls);

  // Calculates where a point on the screen in pixels corresponds to a point in world space.
  // Inverse of WorldSpaceToScreenSpace.
  PointGWS2 ScreenSpaceToWorldSpace(const PointPixels &point);

  // Calculates where a point in world space corresponds to a point on the screen in pixels.
  // Inverse of ScreenSpaceToWorldSpace.
  PointPixels WorldSpaceToScreenSpace(const PointGWS2 &point);

private:
  void RecomputeMatrices();

  // True when NDC depth is in [-1, 1] range, otherwise is [0,1].
  bool m_isHomogeneousDepth = true;
  // True when (-1, -1) in NDC space maps to the bottom left - otherwise, it
  // maps to the top left.
  bool m_isOriginBottomLeft = false;
  // Camera origin in GWS on the XY plane
  Vec2 m_origin{0.5f * 1920.0f / 1080.0f, 0.5f};
  // Size of the output texture in pixels
  PointPixels m_outputSize = {1, 1};
  // Vertical extent (along the Y axis) of the camera
  float m_yExtentWS = 1.0f;
  // Range of z values
  float m_zMin = -1.0f, m_zMax = 1.0f;

  // Cached projection matrix
  Matrix m_proj{};
  // Cachec view matrix
  Matrix m_view{};
};

} // namespace gridby