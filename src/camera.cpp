﻿#include "camera.h"
#include <cmath>

namespace gridby {

void PlanarCamera::RecomputeMatrices() {
  // The view matrix transforms
  // m_origin to (0, 0, 0, 1)
  // and otherwise applies no scaling or rotation.
  m_view = Matrix();
  m_view(0, 3) = -m_origin.x;
  m_view(1, 3) = -m_origin.y;
  m_view(2, 3) = 0.0f;

  // The projection matrix transforms
  //   (0, 0, zMax) to (0, 0, 1)
  //   (0, 0, zMin) to (0, 0, (isHomogeneousDepth?-1:0))
  //   (±(0.5*yExtent*outputW/outputH), 0, zMax) to (±1, 0, 1)
  //   (0, ±0.5*yExtent, zMax) to (0, ∓1, 1)
  // so, it's equal to
  //   (2*outputH/(outputW*yExtent),          0,  0,  0)
  //   (                          0, -2/yExtent,  0,  0)
  //   (                          0,          0, z3, w3)
  //   (                          0,          0,  0,  1)
  // where
  //   z3 zMin + w3 == (isHomogeneousDepth?-1:0)
  //   z3 zMax + w3 == 1
  // ->
  //   z3 = (isHomogeneousDepth?2:1)/(zMax-zMin)
  //   w3 = 1 - z3 zMax
  m_proj = Matrix();
  m_proj(0, 0) = 2.0f * m_outputSize.y / (m_outputSize.x * m_yExtentWS);
  m_proj(1, 1) = -2.0f / m_yExtentWS;
  float z3 = (m_isHomogeneousDepth ? 2.0f : 1.0f) / (m_zMax - m_zMin);
  float w3 = 1 - z3 * m_zMax;
  m_proj(2, 2) = z3;
  m_proj(2, 3) = w3;
}

void PlanarCamera::InputControls(const Controls &controls) {
  // This only handles simple translation at the moment.
  const uint8_t mouseButtons = controls.GetImGuiMouseButtons();
  if ((mouseButtons & IMGUI_MBUT_RIGHT) != 0) {
    // Compute how much the camera's y extent should scale as a result of
    // scrolling
    const float yExtentMultiplier =
        powf(0.8f, controls.GetScrollOffsetY<float>());
    const float newYExtent = m_yExtentWS * yExtentMultiplier;
    // The cursor dragged from p0 to p1 in pixels, and the extent changed from
    // yExtent to newYExtent. We would like to find the new camera position
    // such that p0 and p1 represent the same world-space point. To do this,
    // let s be the size of the screen (I think it turns out that the screen's
    // upper-left coordinate doesn't matter!), cPos0 be the current camera
    // position in world-space, and cPos1 be the new camera position. Then we
    // solve the following for cPos1:

    // (p0 - s/2)*extent/s + cPos0 = (p1 - s/2)*extent/s + cPos1
    // -> cPos1 = cPos0 + (p0/s - .5)extent - (p1/s - .5)newExtent.
    PointPixels p0{controls.GetLastMouseX<float>(),
                   controls.GetLastMouseY<float>()};
    PointPixels p1{controls.GetMouseX<float>(), controls.GetMouseY<float>()};
    const float xExtent = m_yExtentWS * m_outputSize.x / m_outputSize.y;
    const float newXExtent = newYExtent * m_outputSize.x / m_outputSize.y;

    const float deltaCPosX = (p0.x / m_outputSize.x - .5f) * xExtent -
                             (p1.x / m_outputSize.x - .5f) * newXExtent;
    const float deltaCPosY = (p0.y / m_outputSize.y - .5f) * m_yExtentWS -
                             (p1.y / m_outputSize.y - .5f) * newYExtent;

    m_origin.x += deltaCPosX;
    m_origin.y += deltaCPosY;
    m_yExtentWS = newYExtent;
    RecomputeMatrices();
  }
}

PointGWS2 PlanarCamera::ScreenSpaceToWorldSpace(const PointPixels &point) {
  // Note: In the future, we could probably do this by directly inverting the
  // projection + view matrix.
  const float xExtent = m_yExtentWS * m_outputSize.x / m_outputSize.y;
  PointGWS2 result;
  result.x = (point.x / m_outputSize.x - .5f) * xExtent + m_origin.x;
  result.y = (point.y / m_outputSize.y - .5f) * m_yExtentWS + m_origin.y;
  return result;
}

PointPixels PlanarCamera::WorldSpaceToScreenSpace(const PointGWS2 &point) {
  // Note: In the future, we could probably do this by directly applying the
  // projection + view matrix.
  const float xExtent = m_yExtentWS * m_outputSize.x / m_outputSize.y;
  PointPixels result;
  result.x = ((point.x - m_origin.x) / xExtent + .5f) * m_outputSize.x;
  result.y = ((point.y - m_origin.y) / m_yExtentWS + .5f) * m_outputSize.y;
  return result;
}

} // namespace gridby