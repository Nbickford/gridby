#pragma once

// For Gridby, I'm experimenting with giving vectors types or aliases denoting
// the coordinate systems they lie in. Sometimes I get confused about how I'm
// transforming vectors, although usually enough commenting will ensure that I'm
// being consistent.

#include "bx/math.h"

namespace gridby {
// Lightweight Vec2 class. By default, X points to the right, and Y points down.
struct Vec2 {
  float x, y;
};

// Represents a lightweight column-major matrix.
struct Matrix {
  float data[16]{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}; // Set to identity by default
  float &at(int row, int col) { return data[row + 4 * col]; }
  const float &at(int row, int col) const { return data[row + 4 * col]; }
  float &operator()(int row, int col) { return at(row, col); }
  const float &operator()(int row, int col) const { return at(row, col); }
  static Matrix Scaling(bx::Vec3 scale) {
    Matrix result;
    result(0, 0) = scale.x;
    result(1, 1) = scale.y;
    result(2, 2) = scale.z;
    return result;
  }
  static Matrix Translation(bx::Vec3 translation) {
    Matrix result;
    result(0, 3) = translation.x;
    result(1, 3) = translation.y;
    result(2, 3) = translation.z;
    return result;
  }
  Matrix operator*(const Matrix& rightMtx) { Matrix result;
    // AB_(ik) = sum(A_(ij)B_(jk), j)
    for (int i = 0; i < 4; i++) {
      for (int k = 0; k < 4; k++) {
        result(i, k) = at(i, 0) * rightMtx(0, k) + at(i, 1) * rightMtx(1, k) +
                       at(i, 2) * rightMtx(2, k) + at(i, 3) * rightMtx(3, k);
      }
    }
    return result;
  }
};

// Represents a point in Gridby World Space
// The main window includes a 1 x (width/height) rectangle displaying the
// composited video - this rectangle lies in what I'm currently calling _Gridby
// World Space_. This is a right-handed coordinate system, where x points to the
// right, y points downwards, and z points into the screen.
using PointGWS = bx::Vec3;

// Represents a point on the XY plane of Gridby World Space
using PointGWS2 = Vec2;

// Represents a point in screen-space with coordinates in pixels.
using PointPixels = Vec2;

// Represents a point in normalized device coordinates. This depends on the
// graphics API being used (e.g. OpenGL or Vulkan).
using PointNDC = bx::Vec3;
} // namespace gridby