#pragma once

#include <memory>
#include <vector>

#include "gridbyMath.h"

namespace gridby {

struct SubnodeInfo {
  PointGWS2 min;
  PointGWS2 max;
};

// Currently prototyping the GridNode class here
struct GridNode {
  uint32_t columns = 1;
  uint32_t rows = 1;
  // Positions of the column dividers along the X axis,
  // from 0 to 1. Must be in ascending order. Length columns - 1.
  std::vector<float> dividerColFracs;
  // Positions of the row dividers along the Y axis, from 0
  // to 1. Must be in ascending order. Length rows - 1.
  std::vector<float> dividerRowFracs;
  // Contains columns*rows subgrids, unless columns == rows == 1, in which case
  // this is a leaf node and this contains no elements. These are numbered like
  // 0 1 2
  // 3 4 5
  std::unique_ptr<std::vector<GridNode>> elements =
      std::make_unique<std::vector<GridNode>>();

  uint32_t XYToIndex(uint32_t xi, uint32_t yi) const {
    return yi * columns + xi;
  }

  // Gets the world-space bounding box of a subnode, given the bounding box of
  // this node.
  SubnodeInfo GetSubnodeInfo(uint32_t xi, uint32_t yi, const PointGWS2 &min,
                             const PointGWS2 &max) const {
    Vec2 subFracs;
    SubnodeInfo result;
    subFracs.x = (xi == 0 ? 0.0f : dividerColFracs[xi - 1]);
    subFracs.y = (yi == 0 ? 0.0f : dividerRowFracs[yi - 1]);
    result.min = {bx::lerp(min.x, max.x, subFracs.x),
                  bx::lerp(min.y, max.y, subFracs.y)};
    subFracs.x = (xi == columns - 1 ? 1.0f : dividerColFracs[xi]);
    subFracs.y = (yi == rows - 1 ? 1.0f : dividerRowFracs[yi]);
    result.max = {bx::lerp(min.x, max.x, subFracs.x),
                  bx::lerp(min.y, max.y, subFracs.y)};
    return result;
  }

  GridNode &GetSubnode(uint32_t xi, uint32_t yi) const {
    return elements->at(XYToIndex(xi, yi));
  }

  bool IsLeafNode() const { return columns == 1 && rows == 1; }
};

// A `Project` essentially stores the complete state of the project that's
// currently being worked on - its timeline, grids, and so on. It's in some
// sense the data that would be saved and loaded to a project file.
struct Project {
  PointPixels outputSize{1920, 1080}; // Size of the output in pixels
  GridNode debugRootGrid;

  PointGWS2 GetVideoQuadSize() const {
    return {outputSize.x / outputSize.y, 1};
  }
};

} // namespace gridby