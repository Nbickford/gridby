#pragma once

#include "camera.h"
#include "controls.h"
#include "project.h"
#include "shaderCache.h"

namespace gridby {

// All the types of things that can be selected.
enum class SelectedObjectType {
  NONE,    // Nothing is selected; no fields are valid.
  GRIDNODE // `gridNode` is valid.
};

struct GridIndex {
  uint32_t x;
  uint32_t y;
};

struct SelectedObject {
  SelectedObjectType type = SelectedObjectType::NONE;
  std::vector<GridIndex> gridPath; // List of per-node indices of edges in the
                                   // tree. If empty, the root node.
  uint32_t gridNodeDepth =
      0; // Depth of the currently selected node in the tree. 0 if the root
         // node. Max is gridPath.size().
};

class App {
public:
  void Run();

private:
  void UpdateAndRender();
  // Draws the internal edges of the tree of the given node in the bounding box
  // from `topLeftMin` to `bottomRightMax`. Assumes that a vertex and index
  // buffer for a quad have been set, and that the camera view and projection
  // matrix is already set up.
  void DrawGridNodeLines(const GridNode &node, PointGWS2 minTopLeft,
                         PointGWS2 maxBottomRight);

  PlanarCamera m_camera;
  Controls m_controls;
  Project m_project;
  SelectedObject m_selection;
  ShaderCache m_shaderCache;

  bgfx::VertexBufferHandle m_vbh;
  bgfx::IndexBufferHandle m_ibhTriList;
  bgfx::IndexBufferHandle m_ibhLineStrip;
  bgfx::ProgramHandle m_program;
  bgfx::ProgramHandle m_programLines;
};

} // namespace gridby