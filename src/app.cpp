#include <iostream>

#include "app.h"
#include "bgfx/bgfx.h"
#include "bx/math.h"
#define GLFW_EXPOSE_NATIVE_WIN32
#include "GLFW/glfw3native.h"

namespace gridby {

struct PosColorVertex {
  float x;
  float y;
  float z;
  uint32_t abgr;
};

// Rectangle in XY plane with upper-left corner at (0,0)
// 0-----3
// |   / |
// | /   |
// 1-----2
static PosColorVertex rectVertices[] = {{0.0f, 0.0f, 0.0f, 0xff000000},
                                        {0.0f, 1.0f, 0.0f, 0xff00ff00},
                                        {1.0f, 1.0f, 0.0f, 0xff00ffff},
                                        {1.0f, 0.0f, 0.0f, 0xff0000ff}};

static const uint16_t rectTriList[] = {0, 1, 3, 1, 2, 3};

static const uint16_t rectLineStrip[] = {0, 1, 2, 3, 0};

static const uint64_t GB_BGFX_STATE_WRITE_RGBA =
    BGFX_STATE_WRITE_RGB | BGFX_STATE_WRITE_A;

// Recursive function that selects a grid object or nothing from a point in
// Gridby World Space and the grid node's bounding box. If the point is not
// inside the grid, the type will be NONE.
void SelectGridGWS(const PointGWS2 &point, GridNode &node, const PointGWS2 &min,
                   const PointGWS2 &max, SelectedObject &result) {
  // Is this point outside the bounding box?
  if (point.x < min.x || point.x > max.x || point.y < min.y ||
      point.y > max.y) {
    result.type = SelectedObjectType::NONE;
    result.gridPath.clear();
    return;
  }

  // If this node has no children, return it directly
  if (node.IsLeafNode()) {
    result.type = SelectedObjectType::GRIDNODE;
    result.gridNodeDepth = result.gridPath.size();
    return;
  }

  // Find the child node that contains this point, with some trickiness to only
  // compute the bounds of one node.
  // Transform the point to fractional coordinates ([0,1]^2)
  const float fx = (point.x - min.x) / (max.x - min.x);
  const float fy = (point.y - min.y) / (max.y - min.y);
  // Find last xi and yi such that dividerColFracs[xi] <= fx and
  // dividerRowFracs[yi] <= fy (note: this might be faster with binary search,
  // but I think it'll be unusual to have that many rows and columns!)

  // Find index of the column that is the first such that
  // node.dividerColFracs[xi] >= fx, or node.columns - 1 if there is no such
  // element (i.e. we're at the rightmost column)
  uint32_t xi = 0;
  while ((xi < node.columns - 1) && (node.dividerColFracs[xi] < fx)) {
    xi++;
  }
  // Same thing for y
  uint32_t yi = 0;
  while ((yi < node.rows - 1) && (node.dividerRowFracs[yi] < fy)) {
    yi++;
  }

  // Recurse
  result.gridPath.push_back({xi, yi});
  SubnodeInfo dimensions = node.GetSubnodeInfo(xi, yi, min, max);
  SelectGridGWS(point, node.GetSubnode(xi, yi), dimensions.min, dimensions.max,
                result);
  return;
}

void App::Run() {
  const int defaultWindowWidth = 1600;
  const int defaultWindowHeight = 900;

  // Initialize GLFW
  glfwInit();
  GLFWwindow *window = glfwCreateWindow(defaultWindowWidth, defaultWindowHeight,
                                        "Gridby", nullptr, nullptr);

  // uint32_t vsyncMode = BGFX_RESET_FLIP_AFTER_RENDER; // TODO: What does this
  // do?
  const uint32_t vsyncMode = BGFX_RESET_VSYNC;

  // Initialize BGFX
  bgfx::Init bgfxInit;                       // Not an aggregate, sadly
  bgfxInit.type = bgfx::RendererType::Count; // Automatically choose a
  // renderer.
  // bgfxInit.type = bgfx::RendererType::OpenGL;
  bgfxInit.resolution.width = defaultWindowWidth;
  bgfxInit.resolution.height = defaultWindowHeight;
  bgfxInit.resolution.reset = vsyncMode;
  bgfxInit.platformData.nwh = glfwGetWin32Window(window);
  bgfx::init(bgfxInit);

  // Initialize Dear ImGui
  imguiCreate();

  // Initialize controls (depends on GLFW and Dear ImGui)
  m_controls.Init(window);

  // Initialize camera
  {
    m_camera.Configure(bgfx::getCaps()->homogeneousDepth,
                       bgfx::getCaps()->originBottomLeft);
    m_camera.SetZBounds(-1.0f, 1.0f);
    m_camera.SetYExtent(2.0f);
  }

  // Create vertex layout
  bgfx::VertexLayout vtxLayout;
  vtxLayout.begin()
      .add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
      .add(bgfx::Attrib::Color0, 4, bgfx::AttribType::Uint8, true)
      .end();
  m_vbh = bgfx::createVertexBuffer(
      bgfx::makeRef(rectVertices, sizeof(rectVertices)), vtxLayout);
  m_ibhTriList =
      bgfx::createIndexBuffer(bgfx::makeRef(rectTriList, sizeof(rectTriList)));
  m_ibhLineStrip = bgfx::createIndexBuffer(
      bgfx::makeRef(rectLineStrip, sizeof(rectLineStrip)));

  // Get shaders
  m_program = m_shaderCache.loadOrCompileProgram("vs.sc", "fs.sc");
  m_programLines = m_shaderCache.loadOrCompileProgram("vs.sc", "fsLines.sc");

  bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x443355FF, 1.0f,
                     0);

  while (!glfwWindowShouldClose(window)) {
    m_controls.Update(window);

    if (m_controls.GetNeedsResize()) {
      bgfx::reset(m_controls.GetWindowWidth<uint32_t>(),
                  m_controls.GetWindowHeight<uint32_t>(), vsyncMode);

      m_camera.SetOutputSize({m_controls.GetWindowWidth<float>(),
                              m_controls.GetWindowHeight<float>()});
    }

    // ImGui
    imguiBeginFrame(m_controls.GetMouseX<int32_t>(),
                    m_controls.GetMouseY<int32_t>(),
                    m_controls.GetImGuiMouseButtons(),
                    m_controls.GetTotalScrollY<int32_t>(),
                    m_controls.GetWindowWidth<uint16_t>(),
                    m_controls.GetWindowHeight<uint16_t>());

    UpdateAndRender();

    imguiEndFrame();
    bgfx::frame();

    // No need for glfwSwapBuffers, I guess?
    glfwPollEvents();
  }

  // Shutdown Dear ImGui and BGFX.
  imguiDestroy();
  bgfx::shutdown();
  glfwDestroyWindow(window);
  glfwTerminate();
}

void App::UpdateAndRender() {
  // Selection
  if (!ImGui::GetIO().WantCaptureMouse) {
    if (m_controls.IsLMBJustPressed()) {
      PointPixels cursorSS = {m_controls.GetMouseX<float>(),
                              m_controls.GetMouseY<float>()};
      // Get mouse position in world space
      PointGWS2 cursorGWS = m_camera.ScreenSpaceToWorldSpace(cursorSS);
      // Select the object at that point
      m_selection.gridPath.clear(); // TODO: Should probably add base case so I
                                    // don't have to do this
      SelectGridGWS(cursorGWS, m_project.debugRootGrid, {0.0f, 0.0f},
                    m_project.GetVideoQuadSize(), m_selection);
    }
  }

  // Camera
  if (!ImGui::GetIO().WantCaptureMouse) {
    m_camera.InputControls(m_controls);
  }

  // GUI
  {
    ImGui::SetNextWindowSize(ImVec2(320, 838), ImGuiCond_FirstUseEver);
    if (ImGui::Begin(ICON_KI_MOVIE " Settings")) {
      ImGui::Text("Gridby version 0.0.0 (built " __DATE__ " at " __TIME__ ")");

      {
        ImGui::AlignTextToFramePadding();
        int width = static_cast<int>(m_project.outputSize.x);
        int height = static_cast<int>(m_project.outputSize.y);
        ImGui::InputInt("Width", &width);
        ImGui::InputInt("Height", &height);
        m_project.outputSize.x =
            static_cast<float>(bx::clamp<int>(width, 1, 8192));
        m_project.outputSize.y =
            static_cast<float>(bx::clamp<int>(height, 1, 8192));
      }

      ImGui::End();
    }

    // Grid node partitioning
    if (m_selection.type == SelectedObjectType::GRIDNODE) {
      // First, handle key presses. The up arrow moves up the tree,
      // and the down arrow moves down the tree.
      if (m_controls.IsKeyJustPressed(GLFW_KEY_UP) &&
          m_selection.gridNodeDepth > 0) {
        m_selection.gridNodeDepth--;
      }
      if (m_controls.IsKeyJustPressed(GLFW_KEY_DOWN) &&
          m_selection.gridNodeDepth < m_selection.gridPath.size()) {
        m_selection.gridNodeDepth++;
      }

      // Get a pointer to the node, and its bounding box
      GridNode *node = &m_project.debugRootGrid;
      PointGWS2 gridNodeMin = {0, 0};
      PointGWS2 gridNodeMax = m_project.GetVideoQuadSize();
      {
        const uint32_t numEdgesToTraverse = m_selection.gridNodeDepth;
        for (uint32_t i = 0; i < numEdgesToTraverse; i++) {
          // If the node is a leaf node, we can't go down any further - the
          // path points to a node that probably no longer exists!
          if (node->IsLeafNode()) {
            break;
          }
          const GridIndex &edge = m_selection.gridPath[i];
          SubnodeInfo subnodeInfo =
              node->GetSubnodeInfo(edge.x, edge.y, gridNodeMin, gridNodeMax);
          gridNodeMin = subnodeInfo.min;
          gridNodeMax = subnodeInfo.max;
          node = &(node->GetSubnode(edge.x, edge.y));
        }
      }
      assert(node != nullptr);

      // Convert bounding box coordinates to screen space
      PointPixels min = m_camera.WorldSpaceToScreenSpace(gridNodeMin);
      PointPixels max = m_camera.WorldSpaceToScreenSpace(gridNodeMax);
      assert(node != nullptr);
      uint32_t oldColumns = node->columns;
      uint32_t oldRows = node->rows;
      // Draw ImGui widgets, in the middle of the sides.
      // This directly changes *node.
      // .------.
      // |      |
      // |      | text
      // |      |
      // *------*
      //   text
      ImGuiWindowFlags windowFlags =
          ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize |
          ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar;
      ImGui::SetNextWindowPos(ImVec2(bx::lerp(min.x, max.x, 0.5f), max.y),
                              ImGuiCond_Always, ImVec2(0.5f, 0.0f));
      if (ImGui::Begin("Grid Columns", nullptr, windowFlags)) {
        // Cast to an int so that we can limit to values >= 1
        int tempColumns = static_cast<int>(node->columns);
        ImGui::SetNextItemWidth(100.0f);
        // Using ImGuiInputTextFlags_NoUndoRedo seems to get around something
        // where switching to a different node will copy over some of the old
        // values
        ImGui::InputInt("##cols", &tempColumns, 1,
                        ImGuiInputTextFlags_NoUndoRedo);
        // Limit to 128, otherwise awful things might happen (on the rendering
        // side, of all places!)
        node->columns = static_cast<uint32_t>(bx::clamp(tempColumns, 1, 128));
        ImGui::End();
      }

      ImGui::SetNextWindowPos(ImVec2(max.x, bx::lerp(min.y, max.y, 0.5f)),
                              ImGuiCond_Always, ImVec2(0.0f, 0.5f));
      if (ImGui::Begin("Grid Rows", nullptr, windowFlags)) {
        int tempRows = static_cast<int>(node->rows);
        ImGui::SetNextItemWidth(100.0f);
        ImGui::InputInt("##rows", &tempRows, 1, ImGuiInputTextFlags_NoUndoRedo);
        node->rows = static_cast<uint32_t>(bx::clamp(tempRows, 1, 128));
        ImGui::End();
      }

      const uint32_t newColumns = node->columns;
      const uint32_t newRows = node->rows;
      if (oldColumns != newColumns || oldRows != newRows) {
        // Repartition the node.
        // We could do this in-place with a lot more trickery. But since we're
        // prototyping this for now, I'm just going to create a new element
        // vector, move over all of the items, and then swap the vectors.
        // Note that this still kind of relies on how elements are laid out in
        // memory, sadly.
        uint32_t newNumElements = newColumns * newRows;

        if (newNumElements == 1) {
          // This is now a leaf node.
          // We could replace this node with its first child, but at the moment
          // this is too tricky, and the node layout might change in the future.
          node->elements->clear();
        } else {
          std::unique_ptr<std::vector<GridNode>> newElements =
              std::make_unique<std::vector<GridNode>>(newNumElements);
          // Move everything that can be moved, if there is something to move
          if (!(oldColumns == 1 && oldRows == 1)) {
            for (uint32_t yi = 0; yi < oldRows; yi++) {
              for (uint32_t xi = 0; xi < oldColumns; xi++) {
                const uint32_t oldIndex = yi * oldColumns + xi;
                const uint32_t newIndex = node->XYToIndex(xi, yi);
                if (newIndex < newNumElements) {
                  newElements->at(newIndex) =
                      std::move(node->elements->at(oldIndex));
                }
              }
            }
          }
          node->elements = std::move(newElements);
        }

        // Set new row and column fractions if each corresponding quantity
        // changed. I'll do this a bit trickily again and scale them uniformly
        // towards the top-left, because I think it might be interesting.
        if (oldColumns != newColumns) {
          node->dividerColFracs.resize(newColumns - 1);
          const float fracScale =
              static_cast<float>(oldColumns) / static_cast<float>(newColumns);
          for (uint32_t xi = 0; xi < newColumns - 1; xi++) {
            if (xi < oldColumns - 1) {
              // Scale the first columns
              node->dividerColFracs[xi] *= fracScale;
            } else {
              // Equally space the rest
              node->dividerColFracs[xi] =
                  static_cast<float>(xi + 1) / static_cast<float>(newColumns);
            }
          }
        }

        // Same for rows.
        if (oldRows != newRows) {
          node->dividerRowFracs.resize(newRows - 1);
          const float fracScale =
              static_cast<float>(oldRows) / static_cast<float>(newRows);
          for (uint32_t yi = 0; yi < newRows - 1; yi++) {
            if (yi < oldRows - 1) {
              // Scale the first rows
              node->dividerRowFracs[yi] *= fracScale;
            } else {
              // Equally space the rest
              node->dividerRowFracs[yi] =
                  static_cast<float>(yi + 1) / static_cast<float>(newRows);
            }
          }
        }
      }
    }

    // ImGui::ShowDemoWindow();
  }

  // Rendering
  bgfx::setViewRect(0, 0, 0, m_controls.GetWindowWidth<uint16_t>(),
                    m_controls.GetWindowHeight<uint16_t>());
  // Make sure that view 0 is cleared if no other draw calls are submitted
  // to view 0.
  bgfx::touch(0);

  // The video mesh is a quad with size (w/h, 1) with upper-left corner at
  // (0,0).
  bgfx::setState(BGFX_STATE_DEFAULT);
  const PointGWS2 videoSize = m_project.GetVideoQuadSize();
  const Matrix videoObjToWorld =
      Matrix::Scaling({videoSize.x, videoSize.y, 1.0f});
  bgfx::setTransform(videoObjToWorld.data);

  const Matrix &view = m_camera.GetViewMatrix();
  const Matrix &proj = m_camera.GetProjectionMatrix();
  bgfx::setViewTransform(0, view.data, proj.data);

  bgfx::setVertexBuffer(0, m_vbh);
  bgfx::setIndexBuffer(m_ibhTriList);
  bgfx::submit(0, m_program);

  // Draw the edges of the grid
  bgfx::setState(BGFX_STATE_PT_LINESTRIP | GB_BGFX_STATE_WRITE_RGBA);
  bgfx::setVertexBuffer(0, m_vbh);
  bgfx::setIndexBuffer(m_ibhLineStrip);
  bgfx::setTransform(videoObjToWorld.data);
  bgfx::submit(0, m_programLines);

  // Draw the lines of the grid
  DrawGridNodeLines(m_project.debugRootGrid, {0, 0}, videoSize);
}

void App::DrawGridNodeLines(const GridNode &node, PointGWS2 minTopLeft,
                            PointGWS2 maxBottomRight) {
  // WARNING: This is *super* slow! Ideally, this would be implemented using
  // instancing and a list of vertices, instead of scaled versions of the debug
  // rectangle as we have it now. This makes it a reasonable target for graphics
  // optimization.

  // Draw lines for this node
  // Column separators
  const float lineHeight = maxBottomRight.y - minTopLeft.y;
  for (uint32_t colSeparator = 0; colSeparator < node.columns - 1;
       colSeparator++) {
    const float frac = node.dividerColFracs[colSeparator];
    const float x = bx::lerp(minTopLeft.x, maxBottomRight.x, frac);
    const Matrix world = Matrix::Translation({x, minTopLeft.y, 0.0f}) *
                         Matrix::Scaling({0.0f, lineHeight, 1.0f});
    bgfx::setState(BGFX_STATE_PT_LINESTRIP | GB_BGFX_STATE_WRITE_RGBA);
    bgfx::setVertexBuffer(0, m_vbh);
    bgfx::setIndexBuffer(m_ibhLineStrip);
    bgfx::setTransform(world.data);
    bgfx::submit(0, m_programLines);
  }

  // Row separators
  const float lineWidth = maxBottomRight.x - minTopLeft.x;
  for (uint32_t rowSeparator = 0; rowSeparator < node.rows - 1;
       rowSeparator++) {
    const float frac = node.dividerRowFracs[rowSeparator];
    const float y = bx::lerp(minTopLeft.y, maxBottomRight.y, frac);
    const Matrix world = Matrix::Translation({minTopLeft.x, y, 0.0f}) *
                         Matrix::Scaling({lineWidth, 0.0f, 1.0f});
    bgfx::setState(BGFX_STATE_PT_LINESTRIP | GB_BGFX_STATE_WRITE_RGBA);
    bgfx::setVertexBuffer(0, m_vbh);
    bgfx::setIndexBuffer(m_ibhLineStrip);
    bgfx::setTransform(world.data);
    bgfx::submit(0, m_programLines);
  }

  // Recurse over nodes
  if (node.rows > 1 || node.columns > 1) {
    for (uint32_t yi = 0; yi < node.rows; yi++) {
      for (uint32_t xi = 0; xi < node.columns; xi++) {
        SubnodeInfo aabb =
            node.GetSubnodeInfo(xi, yi, minTopLeft, maxBottomRight);
        // DEBUG: Drawing a quad for each node
        const Matrix objToWorld =
            Matrix::Translation({aabb.min.x, aabb.min.y, 0.0f}) *
            Matrix::Scaling(
                {aabb.max.x - aabb.min.x, aabb.max.y - aabb.min.y, 0.0f});
        bgfx::setState(GB_BGFX_STATE_WRITE_RGBA);
        bgfx::setTransform(objToWorld.data);
        bgfx::setVertexBuffer(0, m_vbh);
        bgfx::setIndexBuffer(m_ibhTriList);
        bgfx::submit(0, m_program);
        // END DEBUG

        DrawGridNodeLines(node.GetSubnode(xi, yi), aabb.min, aabb.max);
      }
    }
  }
}

} // namespace gridby