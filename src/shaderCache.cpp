#include <assert.h>
#include <filesystem>
#include <fstream>
#include <sstream>

#include "bx/process.h"
#include "shaderCache.h"

namespace gridby {

bgfx::ShaderHandle ShaderCache::loadOrCompileShader(std::string shaderName,
                                                    ShaderType type) {
  // TODO: Make this actually cache, and use IDs so that we can
  // hot-reload shaders!
  std::string compiledShaderDir;
  switch (bgfx::getRendererType()) {
  case bgfx::RendererType::Direct3D11:
    compiledShaderDir = "shaders/dx11";
    break;
  case bgfx::RendererType::OpenGL:
    compiledShaderDir = "shaders/gl";
    break;
  default:
    assert(!"Compiled shader directory not implemented for renderer - need to "
            "modify loadOrCompileShader!");
    break;
  }
  const std::string compiledShaderPath =
      compiledShaderDir + "/" + shaderName + ".bin";
  const std::string srcShaderPath =
      std::string(GRIDBY_DEV_SHADER_PATH) + "/" + shaderName;
  const std::string varyingDefPath =
      std::string(GRIDBY_DEV_SHADER_PATH) + "/" + "varying.def.sc";

  // Create the output directory if it doesn't already exist
  std::filesystem::create_directory("shaders");
  std::filesystem::create_directory(compiledShaderDir);

  // A bit of a hack, based off of
  // https://github.com/bkaradzic/bgfx/issues/405#issuecomment-284258482
  bx::ProcessReader processReader;
  bx::Error error;
  std::stringstream arguments;
  arguments << std::string("-f ") + srcShaderPath                //
            << std::string(" --varyingdef ") + varyingDefPath    //
            << std::string(" --platform windows")                //
            << std::string(" -i ") << GRIDBY_DEV_BGFX_SHADER_INC //
            << std::string(" -o ") + compiledShaderPath;

  if (bgfx::getRendererType() == bgfx::RendererType::Direct3D11) {
    switch (type) {
    case ShaderType::Vertex:
      arguments << " -p vs_5_0";
      break;
    case ShaderType::Fragment:
      arguments << " -p ps_5_0";
      break;
    }
  }

  switch (type) {
  case ShaderType::Vertex:
    arguments << " --type vertex";
    break;
  case ShaderType::Fragment:
    arguments << " --type fragment";
    break;
  }

  const std::string argumentsStr = arguments.str();
  if (!processReader.open(GRIDBY_SHADERC_PATH, argumentsStr.c_str(), &error)) {
    throw std::exception("Unable to load shader file!");
  }

  if (!error.isOk()) {
    std::string message = std::string("Unable to load shader file: ") +
                          error.getMessage().getPtr();
    throw std::exception(message.c_str());
  }

  char buffer[65536]{}; // Initially sets to 0
  assert(sizeof(buffer) == 65536);
  processReader.read(buffer, sizeof(buffer), &error);
  const int32_t result = processReader.getExitCode();
  processReader.close();

  // shaderc only outputs text when there's an error
  if (buffer[0] != 0) {
    std::string message = std::string("Unable to compile shader file: ") +
                          error.getMessage().getPtr() +
                          std::string(" - buffer: ") + buffer;
    throw std::exception(message.c_str());
  }

  // Now load the file from disk
  std::ifstream in(compiledShaderPath, std::ios::in | std::ios::binary);
  if (!in) {
    std::string message =
        std::string("Unable to load shader from disk: ") + compiledShaderPath;
    throw std::exception(message.c_str());
  }

  in.seekg(0, std::ios::end);
  const bgfx::Memory *memory = bgfx::alloc(static_cast<uint32_t>(in.tellg()));
  in.seekg(0, std::ios::beg);
  in.read(reinterpret_cast<char *>(memory->data), memory->size);
  in.close();

  return bgfx::createShader(memory);
}

bgfx::ProgramHandle ShaderCache::loadOrCompileProgram(std::string vsName,
                                                      std::string fsName) {
  bgfx::ShaderHandle vShader = loadOrCompileShader(vsName, ShaderType::Vertex);
  bgfx::ShaderHandle fShader =
      loadOrCompileShader(fsName, ShaderType::Fragment);
  return bgfx::createProgram(vShader, fShader,
                             true); // Destroy shaders when program is destroyed
}

} // namespace gridby