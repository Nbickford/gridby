#pragma once
// This file is named somewhat aspirationally - it doesn't do any in-memory
// caching yet!

#include <string>

#include "bgfx/bgfx.h"

namespace gridby {

enum class ShaderType { Vertex, Fragment };

class ShaderCache {
public:
  bgfx::ShaderHandle loadOrCompileShader(std::string shaderName,
                                         ShaderType type);

  bgfx::ProgramHandle ShaderCache::loadOrCompileProgram(std::string vsName,
                                                        std::string fsName);
};

} // namespace gridby